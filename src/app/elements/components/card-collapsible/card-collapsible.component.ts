import {Component, Input, OnInit} from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'card-collapsible',
    styles: [
        `
            .card-collapsible {
            }
        `
    ],
    template: `
        <div class="card mb-0">
            <div class="card-header bg-white border-0 d-flex">
                <h5 class="w-100 mb-0 text-dark font-weight-normal">
                  <ng-content *ngIf="!alternateHeader || (alternateHeader && collapsed)" select="card-header"></ng-content>
                  <ng-content *ngIf="!collapsed && alternateHeader" select="card-alternate-header"></ng-content>
                </h5>
                <button (click)="collapsed = !collapsed"
                        class="flex-shrink-1  btn btn-link collapsed float-right text-secondary text-decoration-none p-0 pl-2"
                        type="button">
                    <span class="d-none d-sm-inline">{{ (collapsed ? 'Expandable.More' : 'Expandable.Less') | translate}}</span>
                    <i class="ml-1 pt-1 fa" [ngClass]="collapsed ? 'fa-chevron-down' : 'fa-chevron-up'"></i>
                </button>
            </div>
            <div *ngIf="!collapsed">
                <div class="card-body pt-0">
                    <ng-content select="card-body"></ng-content>
                </div>
            </div>
        </div>
    `
})
/**
 * @description Implements a collapsible card component.
 * @example
 <card-collapsible [collapsed]="false">
     <card-header>Card title</card-header>
     <card-body>
         <div>
            The quick brown fox jumps over the lazy dog
         </div>
     </card-body>
 </card-collapsible>
 */
export class CardCollapsibleComponent implements OnInit {

    /**
     * Gets or sets a boolean which indicates whether this card is collapsed or
     */
    @Input() collapsed = true;
    @Input() alternateHeader = false;
    ngOnInit(): void {
        //
    }

}
