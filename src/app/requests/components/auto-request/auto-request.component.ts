import { Component, OnInit, Input , ViewChild, Output} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {GradeScale, ModalService} from '@universis/common';
import { ElementRef, TemplateRef } from '@angular/core';
import {RequestsService} from '../../services/requests.service';
import {MessageSharedService} from '../../../students-shared/services/messages.service';
import { LoadingService } from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {ProfileService} from '../../../profile/services/profile.service';
import {ActivatedRoute, Router} from '@angular/router';



@Component({
  selector: 'app-auto-request',
  templateUrl: './auto-request.component.html',
  styleUrls: ['./auto-request.component.scss']
})
export class AutoRequestComponent implements OnInit {
  @ViewChild('templateSendRequest') sendRequestTemplate: TemplateRef<any>;
  @ViewChild('templateFail') failTemplate: TemplateRef<any>;

  @Input() public requestType: any;
  @Input() public url: any;

  public checkRequest: any;
  public documentRequests: any = [];
  public availableRequestType  = true;
  private modalRef: any;
  public isLoading = true;
  public hasError = false;

  constructor(private requestsService: RequestsService,
              private modalService: ModalService,
              private _context: AngularDataContext,
              private loadingService: LoadingService,
              private translate: TranslateService,
              private _profileService: ProfileService,
              private _router: Router,
              private contextService: AngularDataContext,
              private messageSharedService: MessageSharedService) { }

  ngOnInit() {
    this.requestsService.getDocumentTypes().then ( (checkrequest) => {
      this.checkRequest =  checkrequest.find ( x => {
        return x.alternateName === this.requestType;
      });
      if (this.checkRequest == null) {
        this.availableRequestType = false;
      }
     // this.loadingService.hideLoading();
      this.isLoading = false;
    });
  }

  sendRequest(title: string) {
    this.loadingService.showLoading();
    this._context.model('RequestDocumentActions').save({
      alternateName: title,
      object: {
        alternateName: title
      },
    })
      .then(request => {
        this.isLoading = true;
        this.hasError = false;
        this.loadingService.hideLoading();
        this.requestsService.getDocumentRequest(request.id).then((documentRequests) => {
          this.documentRequests = documentRequests;
          if (this.documentRequests.messages.length > 0) {
            this.modalService.showDialog(
              this.translate.instant('NewRequestTemplates.' + title + '.Title'),
              this.translate.instant(this.documentRequests.messages[0].body)
            ).then(() => {
              this.messageSharedService.callUnReadMessages();
// navigate to other url only if request is completed
              if (this.documentRequests.actionStatus.alternateName === 'CompletedActionStatus') {
                return this._router.navigate([this.url]);
              }
            });
          }
        });
      }).catch((err) => {
      this.hasError = true;
      this.isLoading = false;
      this.loadingService.hideLoading();
      this.modalRef = this.modalService.openModal(this.failTemplate, 'modal-lg');
    });
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.openModal(template, 'modal-md');
  }

  closeModal() {
    // Reload the page. A more specific function may be implemented later
    this.ngOnInit();
    this.modalRef.hide();
  }

}
